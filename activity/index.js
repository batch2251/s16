// // ACTIVITY

// /*
// 	1. Debug the following code to return the correct value and mimic the output.
// */

	let num1 = 25;
	let num2 = 5;
	let sum = num1 + num2;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result: " + sum);
	

	let num3 = 156;
	let num4 = 44;
	let add = num3 + num4;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result: " + add);
	

	let num5 = 17;
	let num6 = 10;
	let difference = num5 - num6;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result: " + difference);
	

	// MINI ACTIVITY


// 2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.



let resultMinutes = 0
    let minutesHour = 60;
    let hoursDay = 24;
    let daysWeek = 7;
    let weeksMonth = 4;
    let monthsYear = 12;
    let daysYear = 365;
resultMinutes = 60 * 24 * 365

console.log("There are " + resultMinutes + " minutes in a year.");



//     3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.

let resultFahrenheit = 0
    let tempCelsius = 132;

resultFahrenheit = (132 * 9/5) + 32;
console.log(tempCelsius + " degrees Celcius when converted to Fahrenheit is: " + resultFahrenheit);


	// 4a. Given the values below, identify if the values of the following variable are divisible by 8.
	//    -Use a modulo operator to identify the divisibility of the number to 8.
	//    -Save the result of the operation in an appropriately named variable.
	//    -Log the value of the remainder in the console.
	//    -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
	//    -Log a message in the console if num7 is divisible by 8.
	//    -Log the value of isDivisibleBy8 in the console.


	let num7 = 165;
	let num88 = 8;
	let remainder = num7 % num88;
	let isDivisibleBy8 = remainder === 0
	//Log the value of the remainder in the console.
	console.log("Remainder is equals: " + remainder);
	console.log("Is num7 divisible by 8? ");
	console.log(isDivisibleBy8);

	

	// //Log the value of isDivisibleBy8 in the console.



	// 4b. Given the values below, identify if the values of the following variable are divisible by 4.
	//    -Use a modulo operator to identify the divisibility of the number to 4.
	
	
	//    -Save the result of the operation in an appropriately named variable.
	//    -Log the value of the remainder in the console.
	//    -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
	//    -Log a message in the console if num8 is divisible by 4.
	//    -Log the value of isDivisibleBy4 in the console.


	let num8 = 348;
	let num9 = 4;
	remainder2 = num8 % num9;
	let isDivisibleBy4 = remainder2 === 0
	console.log("Remainder is equals: " + remainder2);
	console.log("Is num8 divisible by 4?");
	console.log(isDivisibleBy4);

	//Log the value of the remainder in the console.
	
	//Log the value of isDivisibleBy4 in the console.

